export default interface GroceriesApiInterface {
    getItems(): Promise<Category[]>;
    addItem(item: Item): Promise<null>;
    addToCart(itemId: number): Promise<null>;
    updateItem(item: Item): Promise<null>;
    deleteItem(item: Item): Promise<null>;

    move(itemId: number, reorder: Move): Promise<null>;
    getHistory(): Promise<Item[]>;
    restore(itemId: number): Promise<null>;
    getLogs(): Promise<Log[]>;
    parseQuery(query: string): Promise<Item[]>;
}
