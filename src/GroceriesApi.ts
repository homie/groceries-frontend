import GroceriesApiInterface from "GroceriesApiInterface";

export default class GroceriesApi implements GroceriesApiInterface{
    public getItems(): Promise<Category[]> {
        return fetch("http://127.0.0.1:8080/api/items")
            .then(result => result.json());
    }

    public addItem(item: Item): Promise<null> {
        return fetch(
            "http://127.0.0.1:8080/api/add",
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            }
        ).then(() => null);
    }

    public addToCart(itemId: number): Promise<null> {
        return fetch(
            `http://127.0.0.1:8080/api/item/${ itemId }/buy`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        ).then(() => null);
    }

    public updateItem(item: Item): Promise<null> {
        return fetch(
            `http://127.0.0.1:8080/api/item/${ item.id }/edit`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            }
        ).then(() => null);
    }

    public deleteItem(item: Item): Promise<null> {
        return fetch(
            `http://127.0.0.1:8080/api/item/${ item.id }`,
            {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            }
        ).then(() => null);
    }

    public move(itemId: number, move: Move): Promise<null> {
        return fetch(
            `http://127.0.0.1:8080/api/item/${ itemId }/move`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(move),
            }
        ).then(() => null);
    }

    public getHistory(): Promise<Item[]> {
        return fetch(
            `http://127.0.0.1:8080/api/history`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        ).then(result => result.json());
    }

    public restore(itemId: number): Promise<null> {
        return fetch(
            `http://127.0.0.1:8080/api/item/${ itemId }/restore`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        ).then(() => null);
    }

    public getLogs(): Promise<Log[]> {
        return fetch(
            `http://127.0.0.1:8080/api/logs`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        ).then(result => result.json());
    }

    public parseQuery(query: string): Promise<Item[]> {
        let url = new URL('http://127.0.0.1:8080/api/assistant');
        url.searchParams.append('query', query);

        return fetch(
            url.toString(),
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        ).then(result => result.json());
    }
}
