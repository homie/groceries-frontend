type Item = {
    order: number;
    id: number,
    name: string,
    bought: boolean,
    amount: number|null,
    unit: string|null,
    after: string|null,
    before: string|null,
    createdDate: string|null,
    lastModifiedDate: string|null,
}
