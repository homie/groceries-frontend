type Category = {
    label: string,
    date: string,
    overdue: boolean,
    items: Item[],
}
