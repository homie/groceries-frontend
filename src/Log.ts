type Log = {
    id: number,
    action: string,
    item: Item|null,
    createdDate: string|null,
}
