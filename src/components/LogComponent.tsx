import * as React from "react";

type LogProps = {
    log: Log,
}

export default class LogComponent extends React.Component<LogProps, any> {
    render(): React.ReactNode {
        return (
            <li>
                Item: {this.props.log.item.name}, action: {this.props.log.action}, date: {this.props.log.createdDate}
            </li>
        );
    }
}
