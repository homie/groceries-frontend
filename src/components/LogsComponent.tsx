import * as React from "react";
import LogComponent from "components/LogComponent";

type LogsProps = {
    logs: Log[],
}

export default class LogsComponent extends React.Component<LogsProps, any> {
    render(): React.ReactNode {
        return <ul>
            {
                this.props.logs.map((log: Log, index: number) => <LogComponent
                    key={log.id.toString()}
                    log={log}
                />)
            }
        </ul>;
    }
}
