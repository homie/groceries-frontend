import * as React from "react";
import HistoryItemComponent from "components/HistoryItemComponent";

type HistoryProps = {
    items: Item[],
    restore: Function,
}

export default class HistoryComponent extends React.Component<HistoryProps, any> {
    render(): React.ReactNode {
        return <ul>
            {
                this.props.items.map((item: Item, index: number) => <HistoryItemComponent
                    key={item.id.toString()}
                    item={item}
                    restore={this.props.restore}
                />)
            }
        </ul>;
    }
}
