import * as React from "react";
import Autosuggest from "react-autosuggest";

type AssistantProps = {
    parseQuery: Function,
    addItem: Function,
}

type AssistantState = {
    items: Item[],
    query: string,
}

export default class AssistantComponent extends React.Component<AssistantProps, AssistantState> {
    constructor(props: any) {
        super(props);
        this.state = {
            items: [],
            query: '',
        }

        this.updateQuery = this.updateQuery.bind(this);
        this.addItem = this.addItem.bind(this);
        this.clearItems = this.clearItems.bind(this);
        this.getSuggestionValue = this.getSuggestionValue.bind(this);
        this.onChange = this.onChange.bind(this);
        this.renderSuggestion = this.renderSuggestion.bind(this);
    }

    public updateQuery(event: any) {
        const query = event.value;
        this.setState({query: query});

        if (!query) {
            this.setState({items: []});
            return;
        }

        this.props.parseQuery(query).then((items: Item[]) => this.setState({items: items}));
    }

    public addItem(event: any, data: any) {
        this.props.addItem(data.suggestion);
        this.setState({query: '', items: []});

        event.preventDefault();
    }

    public clearItems() {
        this.setState({items: []});
    }

    public getSuggestionValue(suggestion: Item) {
        return (suggestion.amount
            ? suggestion.unit
                ? suggestion.amount + ' ' + suggestion.unit + ' '
                : suggestion.amount + 'x' + ' '
            : '')
            + suggestion.name
            + (suggestion.before
                ? ' na ' + suggestion.before
                : '');
    }

    public renderSuggestion(suggestion: Item) {
        return (
            <span className={'suggestion-content'}>
                {this.getSuggestionValue(suggestion)}
            </span>
        );
    }

    public onChange(event: any, data: any) {
        this.setState({
           query: data.newValue
        });
    }

    public render(): React.ReactNode {
        const inputProps = {
            value: this.state.query,
            onChange: this.onChange
        };
        return (
            <Autosuggest
                suggestions={this.state.items}
                onSuggestionsFetchRequested={this.updateQuery}
                onSuggestionsClearRequested={this.clearItems}
                onSuggestionSelected={this.addItem}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                highlightFirstSuggestion={true}
                inputProps={inputProps}
            />
        );
    }
}
