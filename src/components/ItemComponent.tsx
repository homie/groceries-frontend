import * as React from "react";
import {Draggable} from "react-beautiful-dnd";

type ItemProps = {
    index: number,
    item: Item,
    addToCart: Function,
    editItem: Function,
    deleteItem: Function,
}

export default class ItemComponent extends React.Component<ItemProps> {
    constructor(props: Readonly<ItemProps>) {
        super(props);

        this.addToCart = this.addToCart.bind(this);
        this.editItem = this.editItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    public render(): React.ReactNode {
        return <Draggable draggableId={this.props.item.id.toString()} index={this.props.index}>
            {(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                >
                    {
                        this.props.item.amount
                            ? this.props.item.unit
                                ? <span>{this.props.item.amount} {this.props.item.unit} </span>
                                : <span>{this.props.item.amount}x </span>
                            : ''
                    }
                    {this.props.item.name} (order {this.props.item.order})
                    &nbsp;<button onClick={this.addToCart}>🛒</button>
                    &nbsp;<button onClick={this.editItem}>🖋️</button>
                    &nbsp;<button onClick={this.deleteItem}>🗑️️</button>
                </div>
            )}
        </Draggable>
    }

    public addToCart(): void {
        this.props.addToCart(this.props.item.id);
    }

    public editItem(): void {
        this.props.editItem(this.props.item);
    }

    public deleteItem(): void {
        this.props.deleteItem(this.props.item);
    }
}
