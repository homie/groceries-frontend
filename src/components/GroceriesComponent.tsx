import * as React from "react";
import {ReactNode} from "react";
import {DragDropContext, DropResult} from "react-beautiful-dnd";
import CategoryComponent from "components/CategoryComponent";
import EditItemForm from "components/EditItemForm";
import GroceriesApi from "GroceriesApi";
import GroceriesApiInterface from "GroceriesApiInterface";
import HistoryComponent from "components/HistoryComponent";
import LogsComponent from "components/LogsComponent";
import AssistantComponent from "./AssistantComponent";

type GroceriesComponentState = {
    categories: Category[],
    editedItem: Item,
    historyItems: Item[]|null,
    logs: Log[]|null,
};

export default class GroceriesComponent extends React.Component<any, GroceriesComponentState> {
    private groceriesApi: GroceriesApiInterface;

    constructor(props: any) {
        super(props);
        this.state = {
            categories: [],
            editedItem: null,
            historyItems: null,
            logs: null,
        };

        this.groceriesApi = new GroceriesApi();

        this.addItem = this.addItem.bind(this);
        this.addToCart = this.addToCart.bind(this);
        this.editItem = this.editItem.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.move = this.move.bind(this);
        this.showHistory = this.showHistory.bind(this);
        this.hideHistory = this.hideHistory.bind(this);
        this.restore = this.restore.bind(this);
        this.showLogs = this.showLogs.bind(this);
        this.hideLogs = this.hideLogs.bind(this);
        this.parseQuery = this.parseQuery.bind(this);
    }

    public componentDidMount(): void {
        this.refreshList();
    }

    public refreshList(): void {
        this.groceriesApi.getItems()
            .then(result => {
                this.setState({categories: result})
            });
    }

    public addItem(item: Item) {
        this.groceriesApi.addItem(item).then(() => this.refreshList());
    }

    public render(): ReactNode {
        return <DragDropContext onDragEnd={this.move}>
            {
                this.state.categories.map(
                    (category: Category, index: number) => <CategoryComponent
                        key={index}
                        index={index}
                        category={category}
                        addToCart={this.addToCart}
                        editItem={this.editItem}
                        deleteItem={this.deleteItem}
                    />
                )
            }<br/>
            <AssistantComponent addItem={this.addItem} parseQuery={this.parseQuery}/>
            { this.state.editedItem != null ? <EditItemForm updateItem={this.updateItem} cancelEdit={this.cancelEdit} item={this.state.editedItem}/> : ''}
            <button onClick={this.showHistory}>Show history</button>
            <button onClick={this.hideHistory}>Hide history</button>
            {
                this.state.historyItems != null
                    ? <HistoryComponent
                        items={this.state.historyItems}
                        restore={this.restore}
                    />
                    : ''
            }
            <button onClick={this.showLogs}>Show logs</button>
            <button onClick={this.hideLogs}>Hide logs</button>
            {
                this.state.logs != null
                    ? <LogsComponent logs={this.state.logs} />
                    : ''
            }
        </DragDropContext>;
    }

    public move(result: DropResult): void {
        if (!result.destination) {
            return;
        }

        const sourceCategoryIndex = parseInt(result.source.droppableId);
        const destinationCategoryIndex = parseInt(result.destination.droppableId);

        if (sourceCategoryIndex === destinationCategoryIndex &&
            result.source.index === result.destination.index) {
            return;
        }

        this.groceriesApi.move(parseInt(result.draggableId), {
            date: this.state.categories[destinationCategoryIndex].date,
            order: result.destination.index + 1,
        }).then(() => this.refreshList());
    }

    public addToCart(itemId: number): void {
        this.groceriesApi.addToCart(itemId).then(() => this.refreshList());
    }

    public editItem(item: Item): void {
        this.setState({editedItem: item});
    }

    public updateItem(item: Item): void {
        this.groceriesApi.updateItem(item)
            .then(() => this.setState({editedItem: null}))
            .then(() => this.refreshList());
    }

    public deleteItem(item: Item): void {
        this.groceriesApi.deleteItem(item)
            .then(() => this.refreshList());
    }

    public cancelEdit(): void {
        this.setState({editedItem: null});
    }

    public showHistory(event: React.MouseEvent): void {
        event.preventDefault();
        this.groceriesApi.getHistory().then(items => this.setState({historyItems: items}));
    }

    public hideHistory(event: React.MouseEvent): void {
        event.preventDefault();
        this.setState({historyItems: null});
    }

    public restore(itemId: number): void {
        this.groceriesApi.restore(itemId).then(() => this.refreshList());
    }

    public showLogs(event: React.MouseEvent): void {
        event.preventDefault();
        this.groceriesApi.getLogs().then(logs => this.setState({logs: logs}));
    }

    public hideLogs(event: React.MouseEvent): void {
        event.preventDefault();
        this.setState({logs: null});
    }

    public parseQuery(query: string): Promise<Item[]> {
        return this.groceriesApi.parseQuery(query);
    }
}
