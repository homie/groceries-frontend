import * as React from "react";

type AddItemProps = {
    addItem: Function,
}

type AddItemFormState = {
    amount: number,
    unit: string,
    name: string,
    after: string,
    before: string,
};

export default class AddItemForm extends React.Component<AddItemProps, AddItemFormState> {
    constructor(props: any) {
        super(props);
        this.state = {
            amount: null,
            unit: '',
            name: '',
            after: '',
            before: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    public handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    public handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        this.props.addItem(this.state);
        event.preventDefault();
    }

    public render(): React.ReactNode {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Amount <input type="number" step="any" name="amount" value={this.state.amount || ''} onChange={this.handleChange}/></label><br/>
                <label>Unit <input type="text" name="unit" value={this.state.unit || ''} onChange={this.handleChange}/></label><br/>
                <label>Name <input type="text" name="name" value={this.state.name || ''} onChange={this.handleChange}/></label><br/>
                <label>After <input type="date" name="after" value={this.state.after || ''} onChange={this.handleChange}/></label><br/>
                <label>Before <input type="date" name="before" value={this.state.before || ''} onChange={this.handleChange}/></label><br/>
                <input type="submit" value="Add" />
            </form>
        );
    }
}