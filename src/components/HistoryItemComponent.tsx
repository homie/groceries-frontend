import * as React from "react";

type HistoryItemProps = {
    item: Item,
    restore: Function,
}

export default class HistoryItemComponent extends React.Component<HistoryItemProps, any> {
    constructor(props: Readonly<HistoryItemProps>) {
        super(props);

        this.restore = this.restore.bind(this);
    }

    render(): React.ReactNode {
        return (
            <li>
                {this.props.item.name} bought on {this.props.item.lastModifiedDate}
                &nbsp;<button onClick={this.restore}>🛒</button>
            </li>
        );
    }

    private restore(): void {
        this.props.restore(this.props.item.id);
    }
}
