import * as React from "react";

type EditItemProps = {
    updateItem: Function,
    cancelEdit: Function,
    item: Item,
}

type EditItemFormState = {
    item: Item,
};

export default class EditItemForm extends React.Component<EditItemProps, EditItemFormState> {
    constructor(props: EditItemProps) {
        super(props);
        this.state = {
            item: props.item,
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    public componentDidUpdate(prevProps:Readonly<EditItemProps>): void {
        if (prevProps.item.id === this.props.item.id) {
            return;
        }

        this.setState({item: this.props.item});
    }

    public handleChange(event: React.ChangeEvent<HTMLInputElement>) {
        this.setState({
            item: {
                ...this.state.item,
                [event.target.name]: event.target.value
            }
        })
    }

    public handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        this.props.updateItem(this.state.item);
        event.preventDefault();
    }

    public handleCancel(event: React.MouseEvent<HTMLButtonElement>) {
        this.props.cancelEdit();
        event.preventDefault();
    }

    public render(): React.ReactNode {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>Amount <input type="number" step="any" name="amount" value={this.state.item.amount || ''} onChange={this.handleChange}/></label><br/>
                <label>Unit <input type="text" name="unit" value={this.state.item.unit || ''} onChange={this.handleChange}/></label><br/>
                <label>Name <input type="text" name="name" value={this.state.item.name || ''} onChange={this.handleChange}/></label><br/>
                <label>After <input type="date" name="after" value={this.state.item.after || ''} onChange={this.handleChange}/></label><br/>
                <label>Before <input type="date" name="before" value={this.state.item.before || ''} onChange={this.handleChange}/></label><br/>
                <input type="submit" value="Edit" />
                <button onClick={this.handleCancel}>Cancel</button>
            </form>
        );
    }
}