import * as React from "react";
import {Droppable} from "react-beautiful-dnd";
import ItemComponent from "components/ItemComponent";

type CategoryProps = {
    index: number,
    category: Category,
    addToCart: Function,
    editItem: Function,
    deleteItem: Function,
}

export default class CategoryComponent extends React.Component<CategoryProps> {
    public render(): React.ReactNode {
        return <Droppable droppableId={this.props.index.toString()}>
            {(provided, snapshot) => (
                <div
                    ref={provided.innerRef}
                    {...provided.droppableProps}
                    className={this.props.category.overdue ? 'overdue' : ''}
                >
                    <span>{ this.props.category.label }</span>
                    {
                        this.props.category.items.map((item: Item, index: number) => <ItemComponent
                            key={item.id.toString()}
                            index={index}
                            item={item}
                            addToCart={this.props.addToCart}
                            editItem={this.props.editItem}
                            deleteItem={this.props.deleteItem}
                        />)
                    }
                    {provided.placeholder}
                </div>
            )}
        </Droppable>
    }
}
