import * as React from "react";
import * as ReactDOM from "react-dom";
import GroceriesComponent from "components/GroceriesComponent";

ReactDOM.render(
    <GroceriesComponent />,
    document.getElementById("groceries")
);
